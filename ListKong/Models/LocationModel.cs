﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace ListKong.Models
{
    public class LocationModel
    {
        public long LocationId { get; set; }
        [Required(ErrorMessage = "Location Name is required!")]
        [MinLength(3, ErrorMessage = "Mininum length for location name is 3!")]
        [MaxLength(100, ErrorMessage = "Maximum length for location name is 100!")]
        [Display(Name = "Location Name")]
        public string LocationName { get; set; }

        [Required(ErrorMessage = "Company Name is required!")]
        [Display(Name = "Company Name")]
        public string CompanyName { get; set; }
        public long CompanyId { get; set; }

        [Required(ErrorMessage = "Description is required!")]
        [MinLength(3, ErrorMessage = "Mininum length for location description is 3!")]
        [MaxLength(500, ErrorMessage = "Maximum length for location description is 500!")]
        [Display(Name = "Location Description")]
        public string LocationDescription { get; set; }

        [Required(ErrorMessage = "Location Address is required!")]
        [MinLength(3, ErrorMessage = "Mininum length for location address 1 is 3!")]
        [MaxLength(500, ErrorMessage = "Maximum length for location address 1 is 500!")]
        [Display(Name = "Location Address 1")]
        public string LocationAddress1 { get; set; }

        [Display(Name = "Location Address 2")]
        public string LocationAddress2 { get; set; }

        [Required(ErrorMessage = "Country is required!")]
        [MinLength(2, ErrorMessage = "Mininum length for Country is 2!")]
        [MaxLength(100, ErrorMessage = "Maximum length for Country is 100!")]
        [Display(Name = "Country")]
        public string Country { get; set; }

        [Required(ErrorMessage = "State is required!")]
        [MinLength(2, ErrorMessage = "Mininum length for State is 2!")]
        [MaxLength(100, ErrorMessage = "Maximum length for State is 100!")]
        [Display(Name = "State")]
        public string State { get; set; }

        [Required(ErrorMessage = "City is required!")]
        [MinLength(2, ErrorMessage = "Mininum length for City is 2!")]
        [MaxLength(100, ErrorMessage = "Maximum length for City is 100!")]
        [Display(Name = "City")]
        public string City { get; set; }

        [Required(ErrorMessage = "Zip Code is required!")]
        [MinLength(4, ErrorMessage = "Mininum length for ZipCode is 4!")]
        [MaxLength(6, ErrorMessage = "Maximum length for ZipCode is 6!")]
        [Display(Name = "Zip Code")]
        public string ZipCode { get; set; }

    }
    public class LocationListModel
    {
        [Display(Name = "Company Id")]
        public int CompanyId { get; set; }

        [Display(Name = "Company Name")]
        public string CompanyName { get; set; }
        public long LocationId { get; set; }
        [Display(Name = "Location Name")]
        public string LocationName { get; set; }

        [Display(Name = "Location Description")]
        public string LocationDescription { get; set; }

        [Display(Name = "Location Address 1")]
        public string LocationAddress1 { get; set; }

        [Display(Name = "Location Address 2")]
        public string LocationAddress2 { get; set; }

        [Display(Name = "Country")]
        public string Country { get; set; }
        [Display(Name = "State")]
        public string State { get; set; }
        [Display(Name = "City")]
        public string City { get; set; }
        [Display(Name = "Zip Code")]
        public string ZipCode { get; set; }

        [Display(Name = "Created On")]
        public DateTime CreatedOn { get; set; }

        [Display(Name = "Default Location")]
        public bool IsDefaultLocation { get; set; }
    }
}
