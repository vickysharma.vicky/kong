﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace ListKong.Models
{
    public class InventoryReportModel
    {
       
        public DateTime? startDate { get; set; }
        public DateTime? endDate { get; set; }
        public string startTime { get; set; }
        public string endTime { get; set; }

        public List<InventoryListReportModel> listing { get; set; }
    }
    public class InventoryListReportModel
    {

        public int InvID { get; set; }
        public int InvLocationID { get; set; }
        [DisplayName("Location Name")]
        public string LocationName { get; set; }
        public int InvCompanyID { get; set; }
        [DisplayName("Company Name")]
        public string CompanyName { get; set; }

        [DisplayName("Inventory Name")]
        public string InvName { get; set; }

        [DisplayName("Description")]
        public string InvDesc { get; set; }
        [DisplayName("CreatedOn")]
        public DateTime InvCreatedDate { get; set; }

        public int? InvStatus { get; set; }
        [DisplayName("Status")]
        public string StatusName { get; set; }
        [DisplayName("Created By")]
        public string FullName { get; set; }
        [DisplayName("Email(Created By)")]
        public string userEmail { get; set; }

    }
}
