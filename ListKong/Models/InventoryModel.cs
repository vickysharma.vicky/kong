﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace ListKong.Models
{
    public class InventoryListModel
    {

        public int InvID { get; set; }
        public int InvLocationID { get; set; }
        [DisplayName("Location Name")]
        public string LocationName { get; set; }
        public int InvCompanyID { get; set; }

        [DisplayName("Company Name")]
        public string CompanyName { get; set; }

        [Required(ErrorMessage = "Inventory name is required!")]
        [MinLength(3, ErrorMessage = "Mininum length for inventory name is 3!")]
        [MaxLength(100, ErrorMessage = "Maximum length for inventory name is 100!")]
        [DisplayName("Inventory Name")]
        public string InvName { get; set; }
        [Required(ErrorMessage = "Inventory desc is required!")]
        [MinLength(3, ErrorMessage = "Mininum length for inventory desc is 3!")]
        [MaxLength(500, ErrorMessage = "Maximum length for inventory desc is 500!")]
        [DisplayName("Inventory Desc")]
        public string InvDesc { get; set; }

        [DisplayName("Created On")]
        public DateTime InvCreatedDate { get; set; }

        public int? InvStatus { get; set; }

        [DisplayName("Status")]
        public string StatusName { get; set; }
        public string Thumbnail { get; set; }

    }

    public class InventoryModel
    {

        public int InvID { get; set; }
        public int InvLocationID { get; set; }
        [DisplayName("Location Name")]
        [Required(ErrorMessage = "Location Name is required!")]
        public string LocationName { get; set; }
        public int InvCompanyID { get; set; }

        [DisplayName("Company Name")]
        [Required(ErrorMessage = "Company Name is required!")]
        public string CompanyName { get; set; }

        [Required(ErrorMessage = "Inventory name is required!")]
        [MinLength(3, ErrorMessage = "Mininum length for inventory name is 3!")]
        [MaxLength(100, ErrorMessage = "Maximum length for inventory name is 100!")]
        [DisplayName("Inventory Name")]
        public string InvName { get; set; }
        [Required(ErrorMessage = "Inventory desc is required!")]
        [MinLength(3, ErrorMessage = "Mininum length for inventory desc is 3!")]
        [MaxLength(500, ErrorMessage = "Maximum length for inventory desc is 500!")]
        [DisplayName("Inventory Desc")]
        public string InvDesc { get; set; }

        [DisplayName("Created On")]
        public DateTime InvCreatedDate { get; set; }
        [NotMapped]
        public List<InventoryImageModel> InventoryImages { get; set; }
       // [NotMapped]
        [Required(ErrorMessage = "Inventory Pic 1 is required!")]
        public IFormFile InventoryImage1 { get; set; }
       // [NotMapped]
        //[Required(ErrorMessage = "Inventory Pic 2 is required!")]
        public IFormFile InventoryImage2 { get; set; }
        //[Required(ErrorMessage = "Inventory Pic 3 is required!")]
       // [NotMapped]
        public IFormFile InventoryImage3 { get; set; }
        //[Required(ErrorMessage = "Inventory Pic 4 is required!")]
        //[NotMapped]
        public IFormFile InventoryImage4 { get; set; }
        public IFormFile InventoryImage5 { get; set; }
        public IFormFile InventoryImage6 { get; set; }
        public IFormFile InventoryImage7 { get; set; }
        public IFormFile InventoryImage8 { get; set; }
        public IFormFile InventoryImage9 { get; set; }
        public IFormFile InventoryImage10 { get; set; }
        public IFormFile InventoryImage11 { get; set; }
        public IFormFile InventoryImage12 { get; set; }
        public int InventoryStatus { get; set; }

        [Required(ErrorMessage = "Main Color is required!")]
        [DisplayName("Main Color")]
        public int? MainColorId { get; set; }
        [DisplayName("Other Main Color")]
        public string OtherMainColor { get; set; }

        [DisplayName("Second Color")]
        public int? SecondColorId { get; set; }
        //[Required(ErrorMessage = "Other Secondary Color is required!")]
        [DisplayName("Other Secondary Color")]
        public string OtherSecondaryColor { get; set; }

        [Required(ErrorMessage = "Brand Name is required!")]
        [DisplayName("Brand Name")]        
        public int? InvBrandId { get; set; }

        //[Required(ErrorMessage = "Other Brand Name is required!")]
        [DisplayName("Other Brand Name")]
        public string OtherBrand { get; set; }
        //[Required(ErrorMessage = "Size is required!")]

        
        //public string InvSize { get; set; }
        [DisplayName("Model Name")]
        public string InvModelName { get; set; }



        [DisplayName("Model Number")]
        public string InvModelNumber { get; set; }
        [DisplayName("Shape")]
        public string InvShape { get; set; }
        [Required(ErrorMessage = "Required!")]
        [DisplayName("How many of these items could separate customers purchase?")]
        public int ItemSeparateCustomersPurchase { get; set; }
        [Required(ErrorMessage = "Required!")]
        [DisplayName("How many items are included in one purchase?")]
        public int ItemIncludedInOnePurchase { get; set; }
        //[Required(ErrorMessage = "Descriptor1 is required!")]
        [DisplayName("Descriptor1")]
        public string Descriptor1 { get; set; }
        [DisplayName("Descriptor2")]
        public string Descriptor2 { get; set; }
        [DisplayName("Descriptor3")]
        public string Descriptor3 { get; set; }
        [Required(ErrorMessage = "Height is required!")]
        [RegularExpression("^[0-9]*$", ErrorMessage = "Numeric Only")]
        [DisplayName("Size")]
        public string Height { get; set; }
        [RegularExpression("^[0-9]*$", ErrorMessage = "Numeric Only")]
        [Required(ErrorMessage = "Width is required!")]
        public string Width { get; set; }
        [RegularExpression("^[0-9]*$", ErrorMessage = "Numeric Only")]
        [Required(ErrorMessage = "Depth is required!")]        
        public string Depth { get; set; }
                
        [Required(ErrorMessage = "Weight is required!")]
        [RegularExpression("^[0-9]*$", ErrorMessage = "Numeric Only")]
        [DisplayName("Weight")]
        public string WeightInPound { get; set; }

        [RegularExpression("^[0-9]*$", ErrorMessage = "Numeric Only")]
        public string WeightInInches { get; set; }
        [DisplayName("Any Damage")]
        public string AnyDamage { get; set; }
        [DisplayName("Additional Notes")]
        public string AdditionalNotes { get; set; }
        [Required(ErrorMessage = "Condition is required!")]
        [DisplayName("Condition")]
        public string ConditionId { get; set; }
        [Required(ErrorMessage = "Storage Location is required!")]
        [DisplayName("Storage Location")]
        public string StorageLocationLsle { get; set; }
        [Required(ErrorMessage="*")]
        public string StorageLocationColumn { get; set; }
        [Required(ErrorMessage = "*")]
        public string StorageLocationRow { get; set; }

    }
    public class InventoryEditModel
    {

        public int InvID { get; set; }
        [Required(ErrorMessage = "Location Name is required!")]
        public int InvLocationID { get; set; }
        [DisplayName("Location Name")]
        [Required(ErrorMessage = "Location Name is required!")]
        public string LocationName { get; set; }
        public int InvCompanyID { get; set; }

        [DisplayName("Company Name")]
        [Required(ErrorMessage = "Company Name is required!")]
        public string CompanyName { get; set; }

        [Required(ErrorMessage = "Inventory name is required!")]
        [MinLength(3, ErrorMessage = "Mininum length for inventory name is 3!")]
        [MaxLength(100, ErrorMessage = "Maximum length for inventory name is 100!")]
        [DisplayName("Inventory Name")]
        public string InvName { get; set; }
        [Required(ErrorMessage = "Inventory desc is required!")]
        [MinLength(3, ErrorMessage = "Mininum length for inventory desc is 3!")]
        [MaxLength(500, ErrorMessage = "Maximum length for inventory desc is 500!")]
        [DisplayName("Inventory Desc")]
        public string InvDesc { get; set; }

        [DisplayName("Created On")]
        public DateTime InvCreatedDate { get; set; }
        [NotMapped]
        public List<InventoryImageModel> InventoryImages { get; set; }
       
        public int InventoryStatus { get; set; }

        [Required(ErrorMessage = "Main Color is required!")]
        [DisplayName("Main Color")]
        public int? MainColorId { get; set; }
        [DisplayName("Other Main Color")]
        public string OtherMainColor { get; set; }

        [DisplayName("Second Color")]
        public int? SecondColorId { get; set; }
        [DisplayName("Other Secondary Color")]
        public string OtherSecondaryColor { get; set; }

        [Required(ErrorMessage = "Brand Name is required!")]
        [DisplayName("Brand Name")]
        public int? InvBrandId { get; set; }

        [DisplayName("Other Brand Name")]
        public string OtherBrand { get; set; }

        [DisplayName("Model Name")]
        public string InvModelName { get; set; }



        [DisplayName("Model Number")]
        public string InvModelNumber { get; set; }
        [DisplayName("Shape")]
        public string InvShape { get; set; }
        [Required(ErrorMessage = "Required!")]
        [DisplayName("How many of these items could separate customers purchase?")]
        public int ItemSeparateCustomersPurchase { get; set; }
        [Required(ErrorMessage = "Required!")]
        [DisplayName("How many items are included in one purchase?")]
        public int ItemIncludedInOnePurchase { get; set; }
        //[Required(ErrorMessage = "Descriptor1 is required!")]
        [DisplayName("Descriptor1")]
        public string Descriptor1 { get; set; }
        [DisplayName("Descriptor2")]
        public string Descriptor2 { get; set; }
        [DisplayName("Descriptor3")]
        public string Descriptor3 { get; set; }
        [Required(ErrorMessage = "Height is required!")]
        [RegularExpression("^[0-9]*$", ErrorMessage = "Numeric Only")]
        [DisplayName("Size")]
        public string Height { get; set; }
        [RegularExpression("^[0-9]*$", ErrorMessage = "Numeric Only")]
        [Required(ErrorMessage = "Width is required!")]
        public string Width { get; set; }
        [RegularExpression("^[0-9]*$", ErrorMessage = "Numeric Only")]
        [Required(ErrorMessage = "Depth is required!")]
        public string Depth { get; set; }

        [Required(ErrorMessage = "Weight is required!")]
        [RegularExpression("^[0-9]*$", ErrorMessage = "Numeric Only")]
        [DisplayName("Weight")]
        public string WeightInPound { get; set; }

        [RegularExpression("^[0-9]*$", ErrorMessage = "Numeric Only")]
        public string WeightInInches { get; set; }

        [DisplayName("Any Damage")]
        public string AnyDamage { get; set; }
        [DisplayName("Additional Notes")]
        public string AdditionalNotes { get; set; }
        [Required(ErrorMessage = "Condition is required!")]
        [DisplayName("Condition")]
        public string ConditionId { get; set; }
        [Required(ErrorMessage = "Storage Location is required!")]
        [DisplayName("Storage Location")]
        public string StorageLocationLsle { get; set; }
        [Required(ErrorMessage = "*")]
        public string StorageLocationColumn { get; set; }
        [Required(ErrorMessage = "*")]
        public string StorageLocationRow { get; set; }

    }
    public class InventoryImageModel
    {
        public IFormFile InventoryImage { get; set; }
        public string FileName { get; set; }
        public string FilePath { get; set; }
        public Int64 ImageId { get; set; }

    }
    public class GalleryImageModel
    {
        public Int64 InvId { get; set; }
        public Int64 InvCompanyID { get; set; }
        public IFormFile InventoryImage { get; set; }
    }
    public class GalleryResult
    {
        public string imagePath { get; set; }
        public Int64 imageId { get; set; }
        public string status { get; set; }
    }
}
