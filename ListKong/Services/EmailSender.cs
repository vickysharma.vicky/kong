﻿using MailKit.Net.Smtp;
using MailKit.Security;
using Microsoft.Extensions.Configuration;
using MimeKit;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace ListKong.Services
{
    // This class is used by the application to send email for account confirmation and password reset.
    // For more details see https://go.microsoft.com/fwlink/?LinkID=532713
    public class EmailSender : IEmailSender
    {
        private readonly IConfiguration _configuration;
        public EmailSender(IConfiguration configuration)
        {
            _configuration = configuration;
        }
        public Task SendEmailAsync(string email, string subject, string htmlMessage)
        {
            return Task.CompletedTask;
        }
        //public Task SendEmailAsync(string email, string subject, string messagebody)
        //{
        //    try
        //    {
        //        var message = new MimeMessage();
        //        message.From.Add(new MailboxAddress(_configuration["EmailSettings:Sender"]));
        //        message.To.Add(new MailboxAddress(email));
        //        message.Subject = subject;
        //        message.Body = new TextPart("plain") { Text = messagebody };


        //        using (var client = new SmtpClient())
        //        {
        //            client.Connect(_configuration["EmailSettings:MailServer"], Convert.ToInt32(_configuration["EmailSettings:MailPort"]), SecureSocketOptions.StartTls);
        //            client.Authenticate(_configuration["EmailSettings:Sender"], _configuration["EmailSettings:Password"]);

        //            client.Send(message);
        //            client.Disconnect(true);
        //        }
        //        //client.Credentials = credential;
        //        // client.Host = _configuration["EmailSettings:MailServer"];
        //        //client.Port = int.Parse(_configuration["EmailSettings:MailPort"]);
        //        //client.EnableSsl = true;

        //        //using (var emailMessage = new MailMessage())
        //        //{
        //        //    emailMessage.To.Add(new MailAddress(email));
        //        //    emailMessage.From = new MailAddress(_configuration["EmailSettings:Sender"]);
        //        //    emailMessage.Subject = subject;
        //        //    emailMessage.Body = message;
        //        //    client.Send(emailMessage);
        //        //}
        //    }
        //    catch (Exception ex)
        //    {
        //        var str = ex.Message.ToString();
        //    }
        //    return Task.CompletedTask;
        //}
    }
}
