﻿$(document).ready(function () {
    var modal = document.getElementById("myModal");

    // Get the image and insert it inside the modal - use its "alt" text as a caption
    var img = document.getElementsByClassName("myImg");
    var modalImg = document.getElementById("img01");
    var captionText = document.getElementById("caption");
    $(document).on('click', '.myImg', function () {
        modal.style.display = "block";
        modalImg.src = this.src;
        captionText.innerHTML = this.alt;
    });

    $(document).on('click', '.close_button', function () {
        modal.style.display = "none";
    });
    $(document).on('click', '.approveInv', function () {
        var inventoryId = $(this).data("id");
        alertify.confirm("Are you sure you want to approve this inventory? If you approve this then no one can edit this inventory.", function (e) {
            if (e) {
                if (inventoryId != "") {
                    $.get("/Inventory/ApproveInventory", { id: inventoryId }, function (data) {
                        if (data.result == "success") {
                            location.reload();
                        }
                        else {
                            alertify.error("Some error in approving. Please try later!").setHeader('');
                        }
                    });
                }
            }
        }).setHeader('');
    });
    $(document).on('change', '.ddlcompany', function () {
        var companyId = $(this).val();
        $("#CompanyName").val($(this).text());
        if (companyId != "") {
            $.get("/Inventory/GetLocationByCompany", { id: companyId }, function (data) {
                if (data.result == "success") {
                    debugger;
                    $("#LocationName").val(data.locName);
                    $("#InvLocationID").val(data.locId);
                }
                else {
                    $("#LocationName").val(data.locName);
                    $("#InvLocationID").val(data.locId);
                    alertify.error("Some error in getting location. Please try later!").setHeader('');
                }
            });
        } else {
            $("#LocationName").val(null);
            $("#InvLocationID").val(null);
            $("#CompanyName").val(null);
        }
    });
    $(document).on('click', '.clsAddmore', function () {

        var cnt = $("#hdnImagesCount").val();
        var addHtml = "";
        var newCount = parseInt(cnt) + 1;
        //$(".clsAddmore").after('<input type="button" class="btn btn-danger clsDeleteMore" value="Delete" style="width:10%; margin-left: 20px;"/>');
        $(".clsAddmore").remove();
        addHtml += '<div class="form-group">';
        addHtml += '<label class="control-label"> Inventory Pic ' + newCount + '</label>';
        addHtml += '<div class="row" style="margin-left:0px;">'
        addHtml += '<input type="file" required class="form-control" name="InventoryImages[' + cnt + '].InventoryImage" id="' + newCount + '" style="width:50%;" />'
        addHtml += '<input type="button" class="btn btn-dark clsAddmore" value="Add More" style="width: 10%; margin-left: 20px;" />'
        addHtml += '</div> </div>';
        $("#imagesDiv").append(addHtml);
        $("#hdnImagesCount").val(newCount);
    });
    $(document).on('click', '.clsDeletemore', function () {
        var cntrl = $(this);
        alertify.confirm("Are you sure you want to delete this image?", function (e) {
            if (e) {
                var id = $(cntrl).data("id");
                $.get("/Inventory/DeleteImage", { imageId: id }, function (data) {
                    if (data == "Ok") {
                        $(cntrl).parent().parent().remove();
                    }
                    else {
                        alertify.alert(data).setHeader('');
                    }
                });
            }
        }).setHeader('');

    });
    $(document).on('click', '.clsAddGalleryImage', function () {
        var cntrl = $(this);
        var cnt = $("#hdnImagesCount").val();
        var fileInput = document.getElementById("InventoryImages[" + cnt + "].InventoryImage");
        var file = fileInput.files[0];
        if (file == undefined) {
            $(fileInput).css("border", "solid 1px red");
            return false;
        }
        var addHtml = "";
        alertify.confirm("Are you sure you want to Add this image?", function (e) {
            if (e) {
                var fdata = new FormData();
                var invId = $("#InvID").val();
                var InvCompanyID = $("#InvCompanyID").val();
                var fileInput = document.getElementById("InventoryImages[" + cnt + "].InventoryImage");
                var file = fileInput.files[0];
                fdata.append("InventoryImage", file);
                fdata.append("InvId", invId);
                fdata.append("InvCompanyID", InvCompanyID);
                $.ajax({
                    type: 'post',
                    url: "/Inventory/AddGalleryImage",
                    data: fdata,
                    processData: false,
                    contentType: false
                }).done(function (result) {
                    if (result.status == 'success') {
                        var divHtml = "";
                        divHtml += '<div class="col-md-4" style="margin-top:25px;">';
                        divHtml += '<div style="float:left">';
                        divHtml += '<img src="' + result.imagePath + '" alt="Inventory Image" class="myImg" width="150" height="150" />';
                        divHtml += '</div>';
                        divHtml += '<div style="float:left">';
                        divHtml += '<input type="button" class="btn btn-danger clsDeletemore" data-id="' + result.imageId + '" value="Delete" style="margin-left: 10px;" />';
                        divHtml += '</div>';
                        divHtml += '</div>';
                        $("#invGallery").append(divHtml);

                        $(cntrl).parent().parent().remove();

                        $(".clsAddGalleryImage").remove();
                        addHtml += '<div class="form-group">';
                        addHtml += '<div class="row" style="margin-left:0px;">'
                        addHtml += '<input type="file" required class="form-control" name="InventoryImages[' + cnt + '].InventoryImage" id="InventoryImages[' + cnt + '].InventoryImage" style="width:50%;" />'
                        addHtml += '<input type="button" class="btn btn-primary clsAddGalleryImage" value="Add More" style="width: 10%; margin-left: 20px;" />'
                        addHtml += '</div> </div>';
                        $("#imagesDiv").append(addHtml);
                        $("#hdnImagesCount").val(cnt);

                    }
                    else {
                        alertify.alert("Error in image upload. Please try after some time.").setHeader('');
                    }
                });
            }
        }).setHeader('');


    });
    $(document).on('change', '.ddlMaincolor', function () {
        var text = $(".ddlMaincolor option:selected").text();
        if (text == "Other") {
            $("#otherMainColorDiv").show();
            $("#OtherMainColor").prop('required', true);
        } else {
            $("#otherMainColorDiv").hide();
            $("#OtherMainColor").prop('required', false);
        }
    });
    $(document).on('change', '.ddSecondarylcolor', function () {
        var text = $(".ddSecondarylcolor option:selected").text();
        if (text == "Other") {
            $("#otherSecondaryColorDiv").show();
            $("#OtherSecondaryColor").prop('required', true);
        } else {
            $("#otherSecondaryColorDiv").hide();
            $("#OtherSecondaryColor").prop('required', false);
        }
    });
    $(document).on('change', '.ddlbrand', function () {
        var text = $(".ddlbrand option:selected").text();
        if (text == "Other") {
            $("#otherBrandDiv").show();
            $("#OtherBrand").prop('required', true);
        } else {
            $("#otherBrandDiv").hide();
            $("#OtherBrand").prop('required', false);
        }
    });
});