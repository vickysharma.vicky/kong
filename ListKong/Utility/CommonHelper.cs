﻿using Microsoft.AspNetCore.Mvc.ModelBinding;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Reflection;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace ListKong.Utility
{
    public class CommonHelper
    {
        public IConfiguration Configuration { get; }
        public CommonHelper(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public string GetTokenData(ClaimsIdentity identity, string key)
        {
            try
            {
                return identity.FindFirst(key).Value;
            }
            catch (Exception ex)
            {
                return string.Empty;
            }

        }

        public string GetModelErrorMessages(ModelStateDictionary errors)
        {
            string messages = "";
            foreach (var item in errors)
            {
                for (int j = 0; j < item.Value.Errors.Count; j++)
                {
                    messages += $"<strong>{item.Value.Errors[j].ErrorMessage}</strong><br/>";
                }
            }
            return messages;
        }

        public List<Dictionary<string, string>> ConvertTableToDictionary(DataTable dt)
        {
            List<Dictionary<string, string>> _list = new List<Dictionary<string, string>>();
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                Dictionary<string, string> _row = new Dictionary<string, string>();
                for (int j = 0; j < dt.Columns.Count; j++)
                {
                    _row.Add(dt.Columns[j].ColumnName, dt.Rows[i][dt.Columns[j].ColumnName].ToString());
                }
                _list.Add(_row);
            }
            return _list;
        }

        // Overloaded Function for Null Check DataTable
        public Boolean checkDBNullResponse(DataTable _dt)
        {
            return (_dt == null) ? false : true;
        }

        // Overloaded Function for Null Check DataSet
        public Boolean checkDBNullResponse(DataSet _ds)
        {
            return (_ds == null) ? false : true;
        }

        // Overloaded Function for complete check DataTable
        public Boolean checkDBResponse(DataTable _dt)
        {
            return (_dt == null || _dt.Rows.Count == 0) ? false : true;
        }

        // Overloaded Function for complete check DataSet
        public Boolean checkDBResponse(DataSet _ds)
        {
            return (_ds == null || _ds.Tables.Count == 0) ? false : true;
        }

        public void SendEmail(string UserName, string Code)
        {
            MailMessage mail = new MailMessage();
            SmtpClient SmtpServer = new SmtpClient("smtp.gmail.com");
            string msg = $"Dear User,  Your Password : " + Code + "      Thanks Team Smart Village"; //Use Email Template Here 
            mail.From = new MailAddress("minaxi.87@gmail.com");
            mail.To.Add("raj.sharma.veer@gmail.com");
            mail.Subject = "Forgot Password";
            mail.Body = msg;
            mail.IsBodyHtml = true;
            SmtpServer.Port = 587;//Convert.ToInt16(this.Configuration["Port"]);
            SmtpServer.EnableSsl = true; // this.Configuration["Enablessl"] != "" ? Convert.ToBoolean(this.Configuration["Enablessl"]) : true;
            SmtpServer.Credentials = new NetworkCredential("minaxi.87@gmail.com", "******");
            try
            {
                SmtpServer.Send(mail);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task SendEmail(string UserName, string subject, string Message)
        {
            MailMessage mail = new MailMessage();
            SmtpClient SmtpServer = new SmtpClient("smtp.gmail.com");
            //Use Email Template Here 
            mail.From = new MailAddress("testdgr2019@gmail.com");
            mail.To.Add(UserName);
            mail.Subject = subject;
            mail.Body = Message;
            mail.IsBodyHtml = true;
            SmtpServer.Port = 587;//Convert.ToInt16(this.Configuration["Port"]);
            SmtpServer.EnableSsl = true; // this.Configuration["Enablessl"] != "" ? Convert.ToBoolean(this.Configuration["Enablessl"]) : true;
            SmtpServer.Credentials = new NetworkCredential("testdgr2019@gmail.com", "testdgr@bst2019");
            try
            {
                await SmtpServer.SendMailAsync(mail);
            }
            catch (Exception ex)
            {
            }
        }
    }

    public static class ExtensionMethods
    {
        public static List<T> DataTableToList<T>(this DataTable table) where T : new()
        {
            List<T> list = new List<T>();
            var typeProperties = typeof(T).GetProperties().Select(propertyInfo => new
            {
                PropertyInfo = propertyInfo,
                Type = Nullable.GetUnderlyingType(propertyInfo.PropertyType) ?? propertyInfo.PropertyType
            }).ToList();

            foreach (var row in table.Rows.Cast<DataRow>())
            {
                T obj = new T();
                foreach (var typeProperty in typeProperties)
                {
                    object value = row[typeProperty.PropertyInfo.Name];
                    object safeValue = value == null || DBNull.Value.Equals(value)
                        ? null
                        : Convert.ChangeType(value, typeProperty.Type);

                    typeProperty.PropertyInfo.SetValue(obj, safeValue, null);
                }
                list.Add(obj);
            }
            return list;
        }

        public static string Encrypt(string clearText)
        {
            string EncryptionKey = "MAKV2SPBNI99212";
            byte[] clearBytes = Encoding.Unicode.GetBytes(clearText);
            using (Aes encryptor = Aes.Create())
            {
                Rfc2898DeriveBytes pdb = new Rfc2898DeriveBytes(EncryptionKey, new byte[] { 0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76 });
                encryptor.Key = pdb.GetBytes(32);
                encryptor.IV = pdb.GetBytes(16);
                using (MemoryStream ms = new MemoryStream())
                {
                    using (CryptoStream cs = new CryptoStream(ms, encryptor.CreateEncryptor(), CryptoStreamMode.Write))
                    {
                        cs.Write(clearBytes, 0, clearBytes.Length);
                        cs.Close();
                    }
                    clearText = Convert.ToBase64String(ms.ToArray());
                }
            }
            return clearText;
        }

        public static string Decrypt(string cipherText)
        {
            string EncryptionKey = "MAKV2SPBNI99212";
            byte[] cipherBytes = Convert.FromBase64String(cipherText);
            using (Aes encryptor = Aes.Create())
            {
                Rfc2898DeriveBytes pdb = new Rfc2898DeriveBytes(EncryptionKey, new byte[] { 0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76 });
                encryptor.Key = pdb.GetBytes(32);
                encryptor.IV = pdb.GetBytes(16);
                using (MemoryStream ms = new MemoryStream())
                {
                    using (CryptoStream cs = new CryptoStream(ms, encryptor.CreateDecryptor(), CryptoStreamMode.Write))
                    {
                        cs.Write(cipherBytes, 0, cipherBytes.Length);
                        cs.Close();
                    }
                    cipherText = Encoding.Unicode.GetString(ms.ToArray());
                }
            }
            return cipherText;
        }

    }
}
