﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using ListKong.Models;
using ListKong.Utility;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;

namespace ListKong.Controllers
{
    [Authorize]
    public class InventoryController : Controller
    {
        #region Controller Properties
        private IConfiguration _configuration;
        private CommonHelper _objHelper;
        private SQLGateway _objDataHelper;
        public string cnnect;
        public string keyForEncrypt, encryptedToken, encryptedEmail, formFlag;

        public InventoryController(IConfiguration configuration)
        {
            this._configuration = configuration;
            this._objDataHelper = new SQLGateway(this._configuration.GetConnectionString("DefaultConnection"));
            this._objHelper = new CommonHelper(this._configuration);
            cnnect = this._configuration.GetConnectionString("DefaultConnection");
        }
        #endregion
        public IActionResult Index()
        {
            string companyName = HttpContext.Session.GetString("_CompanyName");
            string companyId = HttpContext.Session.GetString("_CompanyId");
            if (string.IsNullOrEmpty(companyName))
            {
                return RedirectToAction("Index", "Company");
            }
            try
            {
                DataTable _dtResponseP = InventoryList();
                if (this._objHelper.checkDBNullResponse(_dtResponseP))
                {
                    List<InventoryListModel> lst = new List<InventoryListModel>();
                    lst = ExtensionMethods.DataTableToList<InventoryListModel>(_dtResponseP);
                    return View(lst);
                }
            }
            catch (Exception ex)
            {
                string E = ex.Message.ToString();
                return View();
            }
            return View();
        }
        public IActionResult AddInventory()
        {
            string companyName = HttpContext.Session.GetString("_CompanyName");
            string companyId = HttpContext.Session.GetString("_CompanyId");
            if (string.IsNullOrEmpty(companyName))
            {
                return RedirectToAction("Index", "Company");
            }
            InventoryModel model = new InventoryModel();
            model.CompanyName = companyName;
            model.InvCompanyID = Convert.ToInt32(companyId);
            var userId = @User.Claims.FirstOrDefault(c => c.Type == "http://schemas.xmlsoap.org/ws/2005/05/identity/claims/nameidentifier").Value;
            var role = @User.Claims.FirstOrDefault(c => c.Type == "http://schemas.microsoft.com/ws/2008/06/identity/claims/role").Value;
            List<KeyValuePair<string, string>> parameters = new List<KeyValuePair<string, string>>();
            parameters.Add(new KeyValuePair<string, string>("@userId", userId));
            parameters.Add(new KeyValuePair<string, string>("@role", role));
            DataTable dt = _objDataHelper.ExecuteProcedure("app_Get_Company_Location_By_User_Id", parameters);
            if (dt != null && dt.Rows.Count > 0)
            {
                model.InvCompanyID = Convert.ToInt32(dt.Rows[0]["CompanyID"]);
                model.InvLocationID = Convert.ToInt32(dt.Rows[0]["LocID"]);
                model.CompanyName = Convert.ToString(dt.Rows[0]["cCompanyName"]);
                model.LocationName = Convert.ToString(dt.Rows[0]["LocName"]);
            }
            //if (role == "Admin")
            //{
                ViewData["LocationByCompany"] = LocationList();
            //}
            ViewData["Brands"] = BrandList();
            ViewData["Colors"] = ColorList();
            ViewData["InventoryConditionList"] = InventoryConditionList();


            return View(model);
        }

        [HttpPost]
        public IActionResult AddInventory(InventoryModel model)
        {
            var role = @User.Claims.FirstOrDefault(c => c.Type == "http://schemas.microsoft.com/ws/2008/06/identity/claims/role").Value;
            if (!ModelState.IsValid)
            {
               // if (role == "Admin")
               // {
                    ViewData["LocationByCompany"] = LocationList();
                //}
                ViewData["Brands"] = BrandList();
                ViewData["Colors"] = ColorList();
                ViewData["InventoryConditionList"] = InventoryConditionList();
                return View(model);
            }
            try
            {
                var userId = @User.Claims.FirstOrDefault(c => c.Type == "http://schemas.xmlsoap.org/ws/2005/05/identity/claims/nameidentifier").Value;
                List<KeyValuePair<string, string>> parameters = new List<KeyValuePair<string, string>>();
                parameters.Add(new KeyValuePair<string, string>("@userId", userId));
                parameters.Add(new KeyValuePair<string, string>("@companyId", Convert.ToString(model.InvCompanyID)));
                parameters.Add(new KeyValuePair<string, string>("@locationId", Convert.ToString(model.InvLocationID)));
                parameters.Add(new KeyValuePair<string, string>("@inventoryName", model.InvName));
                parameters.Add(new KeyValuePair<string, string>("@inventoryDesc", model.InvDesc));

                parameters.Add(new KeyValuePair<string, string>("@MainColorId", Convert.ToString(model.MainColorId)));
                if (!string.IsNullOrEmpty(model.OtherMainColor))
                    parameters.Add(new KeyValuePair<string, string>("@OtherMainColor", model.OtherMainColor));
                parameters.Add(new KeyValuePair<string, string>("@SecondColorId", Convert.ToString(model.SecondColorId)));
                if (!string.IsNullOrEmpty(model.OtherSecondaryColor))
                    parameters.Add(new KeyValuePair<string, string>("@OtherSecondaryColor", model.OtherSecondaryColor));
                parameters.Add(new KeyValuePair<string, string>("@InvBrandId", Convert.ToString(model.InvBrandId)));
                if (!string.IsNullOrEmpty(model.OtherBrand))
                    parameters.Add(new KeyValuePair<string, string>("@OtherBrand", Convert.ToString(model.OtherBrand)));

                parameters.Add(new KeyValuePair<string, string>("@InvModelName", model.InvModelName));
                parameters.Add(new KeyValuePair<string, string>("@InvModelNumber", model.InvModelNumber));
                parameters.Add(new KeyValuePair<string, string>("@InvShape", model.InvShape));
                parameters.Add(new KeyValuePair<string, string>("@ItemSeparateCustomersPurchase", Convert.ToString(model.ItemSeparateCustomersPurchase)));
                parameters.Add(new KeyValuePair<string, string>("@ItemIncludedInOnePurchase", Convert.ToString(model.ItemIncludedInOnePurchase)));
                parameters.Add(new KeyValuePair<string, string>("@Descriptor1", model.Descriptor1));
                parameters.Add(new KeyValuePair<string, string>("@Descriptor2", model.Descriptor2));
                parameters.Add(new KeyValuePair<string, string>("@Descriptor3", model.Descriptor3));
                parameters.Add(new KeyValuePair<string, string>("@Height", model.Height));
                parameters.Add(new KeyValuePair<string, string>("@Width", model.Width));
                parameters.Add(new KeyValuePair<string, string>("@Depth", model.Depth));
                parameters.Add(new KeyValuePair<string, string>("@WeightInPound", model.WeightInPound));
                parameters.Add(new KeyValuePair<string, string>("@WeightInInches", model.WeightInInches));
                parameters.Add(new KeyValuePair<string, string>("@AnyDamage", model.AnyDamage));
                parameters.Add(new KeyValuePair<string, string>("@AdditionalNotes", model.AdditionalNotes));
                parameters.Add(new KeyValuePair<string, string>("@ConditionId", Convert.ToString(model.ConditionId)));
                parameters.Add(new KeyValuePair<string, string>("@StorageLocationLsle", Convert.ToString(model.StorageLocationLsle)));
                parameters.Add(new KeyValuePair<string, string>("@StorageLocationColumn", Convert.ToString(model.StorageLocationColumn)));
                parameters.Add(new KeyValuePair<string, string>("@StorageLocationRow", Convert.ToString(model.StorageLocationRow)));
                DataTable dt = _objDataHelper.ExecuteProcedure("app_Add_New_Inventory", parameters);
                if (dt != null && dt.Rows.Count > 0)
                {
                    ViewData["SuccessMessage"] = "Inventory successfully added!";
                    model.InvID = Convert.ToInt32(dt.Rows[0]["InventoryID"]);
                }
                DataTable dtTemp = new DataTable();
                dtTemp.Columns.Add("ImageName", typeof(string));
                dtTemp.Columns.Add("ImagePath", typeof(string));
                model.InventoryImages = new List<InventoryImageModel>();
                model.InventoryImages.Add(new InventoryImageModel { InventoryImage = model.InventoryImage1 });
                if (model.InventoryImage2 != null)
                    model.InventoryImages.Add(new InventoryImageModel { InventoryImage = model.InventoryImage2 });
                if (model.InventoryImage3 != null)
                    model.InventoryImages.Add(new InventoryImageModel { InventoryImage = model.InventoryImage3 });
                if (model.InventoryImage4 != null)
                    model.InventoryImages.Add(new InventoryImageModel { InventoryImage = model.InventoryImage4 });

                if (model.InventoryImage5 != null)
                    model.InventoryImages.Add(new InventoryImageModel { InventoryImage = model.InventoryImage5 });
                if (model.InventoryImage6 != null)
                    model.InventoryImages.Add(new InventoryImageModel { InventoryImage = model.InventoryImage6 });
                if (model.InventoryImage7 != null)
                    model.InventoryImages.Add(new InventoryImageModel { InventoryImage = model.InventoryImage7 });
                if (model.InventoryImage8 != null)
                    model.InventoryImages.Add(new InventoryImageModel { InventoryImage = model.InventoryImage8 });
                if (model.InventoryImage9 != null)
                    model.InventoryImages.Add(new InventoryImageModel { InventoryImage = model.InventoryImage9 });
                if (model.InventoryImage10 != null)
                    model.InventoryImages.Add(new InventoryImageModel { InventoryImage = model.InventoryImage10 });
                if (model.InventoryImage11 != null)
                    model.InventoryImages.Add(new InventoryImageModel { InventoryImage = model.InventoryImage11 });
                if (model.InventoryImage12 != null)
                    model.InventoryImages.Add(new InventoryImageModel { InventoryImage = model.InventoryImage12 });
                foreach (var item in model.InventoryImages)
                {
                    string fileName = model.InvID + "_" + DateTime.Now.Day + DateTime.Now.Month + DateTime.Now.Year + '_' + item.InventoryImage.FileName;
                    string FilePath = FileUploadUtility.UploadFile(item.InventoryImage, Convert.ToString(model.InvCompanyID), fileName);
                    dtTemp.Rows.Add(fileName, FilePath);
                }
                using (SqlConnection db = new SQLGateway(cnnect).CreateConnection())
                {
                    using (SqlCommand cmd = new SqlCommand("app_Add_Inventory_Images"))
                    {
                        cmd.Connection = db;
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@InventoryId", model.InvID);
                        cmd.Parameters.AddWithValue("@CreatedBy", userId);
                        cmd.Parameters.AddWithValue("@InventoryImageTableType", dtTemp);
                        db.Open();
                        cmd.ExecuteNonQuery();
                        db.Close();
                        cmd.Dispose();
                    }
                }
                //RedirectToAction("AddInventory");
                TempData["SuccessMessage"] = "Inventory successfully added!";
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                string msg = ex.Message.ToString();
            }
            return View();
        }
        public IActionResult Delete(string Id)
        {            
            try
            {
                DataTable _dtResponseP = DeleteInventory(Id);
                if (this._objHelper.checkDBNullResponse(_dtResponseP))
                {
                    if (Convert.ToString(_dtResponseP.Rows[0]["Result"]) == "1")
                    {
                        TempData["SuccessMessage"] = "Location successfully Deleted!";
                        return RedirectToAction("Index");
                    }
                    else
                    {
                        TempData["ErrorMessage"] = "There is some error. Please try later!";
                        return RedirectToAction("Index");
                    }
                }
                else
                {
                    TempData["ErrorMessage"] = "There is some error. Please try later!";
                    return RedirectToAction("Index");
                }               
            }
            catch (Exception ex)
            {
                TempData["ErrorMessage"] = ex.Message.ToString();
                return RedirectToAction("Index");
            }
        }
        private DataTable DeleteInventory(string Id)
        {
            var userId = @User.Claims.FirstOrDefault(c => c.Type == "http://schemas.xmlsoap.org/ws/2005/05/identity/claims/nameidentifier").Value;
            List<KeyValuePair<string, string>> parameters = new List<KeyValuePair<string, string>>();
            parameters.Add(new KeyValuePair<string, string>("@Id", Id));
            parameters.Add(new KeyValuePair<string, string>("@userId", userId));
            return _objDataHelper.ExecuteProcedure("app_Delete_Inventory_By_Id", parameters);
        }
        public IActionResult Edit(string Id)
        {
            ViewData["Brands"] = BrandList();
            ViewData["Colors"] = ColorList();
            ViewData["InventoryConditionList"] = InventoryConditionList();
            ViewData["LocationByCompany"] = LocationList();
            InventoryEditModel model = new InventoryEditModel();
            List<KeyValuePair<string, string>> parameters = new List<KeyValuePair<string, string>>();
            parameters.Add(new KeyValuePair<string, string>("@InvID", Id));
            DataSet ds = _objDataHelper.ExecuteProcedureWithDataSet("app_Get_Inventory_By_Id", parameters);
            if (ds.Tables.Count > 0)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    model.InvID = Convert.ToInt32(Id);
                    model.CompanyName = Convert.ToString(ds.Tables[0].Rows[0]["CompanyName"]);
                    model.LocationName = Convert.ToString(ds.Tables[0].Rows[0]["LocationName"]);
                    model.InvCompanyID = Convert.ToInt32(ds.Tables[0].Rows[0]["InvCompanyID"]);
                    model.InvLocationID = Convert.ToInt32(ds.Tables[0].Rows[0]["InvLocationID"]);
                    model.InvName = Convert.ToString(ds.Tables[0].Rows[0]["InvName"]);
                    model.InvDesc = Convert.ToString(ds.Tables[0].Rows[0]["InvDesc"]);
                    model.InvCreatedDate = Convert.ToDateTime(ds.Tables[0].Rows[0]["InvCreatedDate"]);
                    model.AdditionalNotes = Convert.ToString(ds.Tables[0].Rows[0]["AdditionalNotes"]);
                    model.AnyDamage = Convert.ToString(ds.Tables[0].Rows[0]["AnyDamage"]);
                    model.ConditionId = Convert.ToString(ds.Tables[0].Rows[0]["InvConditionId"]);
                    model.Depth = Convert.ToString(ds.Tables[0].Rows[0]["Depth"]);
                    model.Descriptor1 = Convert.ToString(ds.Tables[0].Rows[0]["Descriptor1"]);
                    model.Descriptor2 = Convert.ToString(ds.Tables[0].Rows[0]["Descriptor2"]);
                    model.Descriptor3 = Convert.ToString(ds.Tables[0].Rows[0]["Descriptor3"]);
                    model.Height = Convert.ToString(ds.Tables[0].Rows[0]["Height"]);
                    model.InvBrandId = Convert.ToInt32(ds.Tables[0].Rows[0]["InvBrandId"]);
                    model.InvModelName = Convert.ToString(ds.Tables[0].Rows[0]["InvModelName"]);
                    model.InvModelNumber = Convert.ToString(ds.Tables[0].Rows[0]["InvModelNumber"]);
                    model.InvShape = Convert.ToString(ds.Tables[0].Rows[0]["InvShape"]);

                    model.ItemIncludedInOnePurchase = Convert.ToInt32(ds.Tables[0].Rows[0]["ItemIncludedInOnePurchase"]);
                    model.ItemSeparateCustomersPurchase = Convert.ToInt32(ds.Tables[0].Rows[0]["ItemSeparateCustomersPurchase"]);
                    model.MainColorId = Convert.ToInt32(ds.Tables[0].Rows[0]["InvMainColorId"]);
                    model.SecondColorId = Convert.ToInt32(ds.Tables[0].Rows[0]["InvSecondColorId"]);
                    model.WeightInInches = Convert.ToString(ds.Tables[0].Rows[0]["WeightInInches"]);
                    model.WeightInPound = Convert.ToString(ds.Tables[0].Rows[0]["WeightInPounds"]);
                    model.Width = Convert.ToString(ds.Tables[0].Rows[0]["Width"]);
                    model.StorageLocationLsle = Convert.ToString(ds.Tables[0].Rows[0]["StorageLocationLsle"]);
                    model.StorageLocationColumn = Convert.ToString(ds.Tables[0].Rows[0]["StorageLocationColumn"]);
                    model.StorageLocationRow = Convert.ToString(ds.Tables[0].Rows[0]["StorageLocationRow"]);
                }
                if (ds.Tables[1].Rows.Count > 0)
                {
                    model.InventoryImages = new List<InventoryImageModel>();
                    foreach (DataRow item in ds.Tables[1].Rows)
                    {
                        InventoryImageModel imageModel = new InventoryImageModel();
                        imageModel.FileName = "/" + Path.Combine(Convert.ToString(item["ImagePath"]), Convert.ToString(item["ImageName"]));
                        imageModel.FilePath = Convert.ToString(item["ImagePath"]);
                        imageModel.ImageId = Convert.ToInt64(item["ImageId"]);
                        model.InventoryImages.Add(imageModel);
                    }
                }
            }
            return View(model);
        }
        [HttpPost]
        public IActionResult Edit(InventoryEditModel model)
        {
            if (!ModelState.IsValid)
            {
                ViewData["Brands"] = BrandList();
                ViewData["Colors"] = ColorList();
                ViewData["InventoryConditionList"] = InventoryConditionList();
                ViewData["LocationByCompany"] = LocationList();
                return View(model);
            }
            var userId = @User.Claims.FirstOrDefault(c => c.Type == "http://schemas.xmlsoap.org/ws/2005/05/identity/claims/nameidentifier").Value;
            List<KeyValuePair<string, string>> parameters = new List<KeyValuePair<string, string>>();
            parameters.Add(new KeyValuePair<string, string>("@userId", userId));
            parameters.Add(new KeyValuePair<string, string>("@companyId", Convert.ToString(model.InvCompanyID)));
            parameters.Add(new KeyValuePair<string, string>("@locationId", Convert.ToString(model.InvLocationID)));
            parameters.Add(new KeyValuePair<string, string>("@inventoryName", model.InvName));
            parameters.Add(new KeyValuePair<string, string>("@inventoryDesc", model.InvDesc));
            parameters.Add(new KeyValuePair<string, string>("@InvID", Convert.ToString(model.InvID)));

            parameters.Add(new KeyValuePair<string, string>("@MainColorId", Convert.ToString(model.MainColorId)));
            if (!string.IsNullOrEmpty(model.OtherMainColor))
                parameters.Add(new KeyValuePair<string, string>("@OtherMainColor", model.OtherMainColor));
            parameters.Add(new KeyValuePair<string, string>("@SecondColorId", Convert.ToString(model.SecondColorId)));
            if (!string.IsNullOrEmpty(model.OtherSecondaryColor))
                parameters.Add(new KeyValuePair<string, string>("@OtherSecondaryColor", model.OtherSecondaryColor));
            //parameters.Add(new KeyValuePair<string, string>("@InvBrandId", Convert.ToString(model.InvBrandId)));
            parameters.Add(new KeyValuePair<string, string>("@InvBrandId", Convert.ToString(model.InvBrandId)));
            if (!string.IsNullOrEmpty(model.OtherBrand))
                parameters.Add(new KeyValuePair<string, string>("@OtherBrand", Convert.ToString(model.OtherBrand)));

            parameters.Add(new KeyValuePair<string, string>("@InvModelName", model.InvModelName));
            parameters.Add(new KeyValuePair<string, string>("@InvModelNumber", model.InvModelNumber));
            parameters.Add(new KeyValuePair<string, string>("@InvShape", model.InvShape));
            parameters.Add(new KeyValuePair<string, string>("@ItemSeparateCustomersPurchase", Convert.ToString(model.ItemSeparateCustomersPurchase)));
            parameters.Add(new KeyValuePair<string, string>("@ItemIncludedInOnePurchase", Convert.ToString(model.ItemIncludedInOnePurchase)));
            parameters.Add(new KeyValuePair<string, string>("@Descriptor1", model.Descriptor1));
            parameters.Add(new KeyValuePair<string, string>("@Descriptor2", model.Descriptor2));
            parameters.Add(new KeyValuePair<string, string>("@Descriptor3", model.Descriptor3));
            parameters.Add(new KeyValuePair<string, string>("@Height", model.Height));
            parameters.Add(new KeyValuePair<string, string>("@Width", model.Width));
            parameters.Add(new KeyValuePair<string, string>("@Depth", model.Depth));
            parameters.Add(new KeyValuePair<string, string>("@WeightInInches", model.WeightInInches));
            parameters.Add(new KeyValuePair<string, string>("@WeightInPound", model.WeightInPound));
            parameters.Add(new KeyValuePair<string, string>("@AnyDamage", model.AnyDamage));
            parameters.Add(new KeyValuePair<string, string>("@AdditionalNotes", model.AdditionalNotes));
            parameters.Add(new KeyValuePair<string, string>("@ConditionId", Convert.ToString(model.ConditionId)));
            parameters.Add(new KeyValuePair<string, string>("@StorageLocationLsle", Convert.ToString(model.StorageLocationLsle)));
            parameters.Add(new KeyValuePair<string, string>("@StorageLocationColumn", Convert.ToString(model.StorageLocationColumn)));
            parameters.Add(new KeyValuePair<string, string>("@StorageLocationRow", Convert.ToString(model.StorageLocationRow)));

            DataTable dt = _objDataHelper.ExecuteProcedure("app_Update_Inventory", parameters);
            if (dt != null && dt.Rows.Count > 0)
            {
                ViewData["SuccessMessage"] = "Inventory successfully updated!";
                model.InvID = Convert.ToInt32(dt.Rows[0]["InventoryID"]);
            }
            TempData["SuccessMessage"] = "Inventory successfully updated!";
            return RedirectToAction("Index");
        }
        public DataTable LocationList()
        {
            var userId = @User.Claims.FirstOrDefault(c => c.Type == "http://schemas.xmlsoap.org/ws/2005/05/identity/claims/nameidentifier").Value;
            List<KeyValuePair<string, string>> parameters = new List<KeyValuePair<string, string>>();
            parameters.Add(new KeyValuePair<string, string>("@userId", userId));
            return _objDataHelper.ExecuteProcedure("app_Get_All_Location_By_Admin_User", parameters);
        }
        public JsonResult GetLocationByCompany(string id)
        {
            List<KeyValuePair<string, string>> parameters = new List<KeyValuePair<string, string>>();
            parameters.Add(new KeyValuePair<string, string>("@companyId", Convert.ToString(id)));
            DataTable dt = _objDataHelper.ExecuteProcedure("app_Get_Location_By_Company_Id", parameters);
            string locationName = "";
            string locationId = "";
            string result = "error";
            if (dt != null && dt.Rows.Count > 0)
            {
                result = "success";
                locationName = Convert.ToString(dt.Rows[0]["LocName"]);
                locationId = Convert.ToString(dt.Rows[0]["LocID"]);
            }
            return Json(new { result = result, locName = locationName, locId = locationId });
        }
        public JsonResult ApproveInventory(string id)
        {
            var userId = @User.Claims.FirstOrDefault(c => c.Type == "http://schemas.xmlsoap.org/ws/2005/05/identity/claims/nameidentifier").Value;
            List<KeyValuePair<string, string>> parameters = new List<KeyValuePair<string, string>>();
            parameters.Add(new KeyValuePair<string, string>("@invId", Convert.ToString(id)));
            parameters.Add(new KeyValuePair<string, string>("@userId", Convert.ToString(userId)));
            DataTable dt = _objDataHelper.ExecuteProcedure("app_Approve_Inventory_By_Id", parameters);
            string result = "error";
            if (dt != null && dt.Rows.Count > 0)
            {
                result = "success";
            }
            return Json(new { result = result });
        }
        public string DeleteImage(Int64 imageId)
        {
            string result = "Error";
            try
            {
                List<KeyValuePair<string, string>> parameters = new List<KeyValuePair<string, string>>();
                parameters.Add(new KeyValuePair<string, string>("@imageId", Convert.ToString(imageId)));
                DataTable dt = _objDataHelper.ExecuteProcedure("app_Delete_Inventory_Image_By_Id", parameters);
                if (dt != null && dt.Rows.Count > 0)
                {
                    result = "Ok";
                }
            }
            catch (Exception ex)
            {
                result = ex.Message.ToString();
            }
            return result;
        }
        public IActionResult Gallery(string Id)
        {
            InventoryModel model = new InventoryModel();
            List<KeyValuePair<string, string>> parameters = new List<KeyValuePair<string, string>>();
            parameters.Add(new KeyValuePair<string, string>("@InvID", Id));
            DataSet ds = _objDataHelper.ExecuteProcedureWithDataSet("app_Get_Inventory_By_Id", parameters);
            if (ds.Tables.Count > 0)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    model.InvID = Convert.ToInt32(Id);
                    model.CompanyName = Convert.ToString(ds.Tables[0].Rows[0]["CompanyName"]);
                    model.LocationName = Convert.ToString(ds.Tables[0].Rows[0]["LocationName"]);
                    model.InvCompanyID = Convert.ToInt32(ds.Tables[0].Rows[0]["InvCompanyID"]);
                    model.InvLocationID = Convert.ToInt32(ds.Tables[0].Rows[0]["InvLocationID"]);
                    model.InvName = Convert.ToString(ds.Tables[0].Rows[0]["InvName"]);
                    model.InvDesc = Convert.ToString(ds.Tables[0].Rows[0]["InvDesc"]);
                    model.InvCreatedDate = Convert.ToDateTime(ds.Tables[0].Rows[0]["InvCreatedDate"]);
                    model.InventoryStatus = Convert.ToInt32(ds.Tables[0].Rows[0]["InvStatus"]);
                }
                if (ds.Tables[1].Rows.Count > 0)
                {
                    model.InventoryImages = new List<InventoryImageModel>();
                    foreach (DataRow item in ds.Tables[1].Rows)
                    {
                        InventoryImageModel imageModel = new InventoryImageModel();
                        imageModel.FileName = "/" + Path.Combine(Convert.ToString(item["ImagePath"]), Convert.ToString(item["ImageName"]));
                        imageModel.FilePath = Convert.ToString(item["ImagePath"]);
                        imageModel.ImageId = Convert.ToInt64(item["ImageId"]);
                        model.InventoryImages.Add(imageModel);
                    }
                }
                else
                {
                    model.InventoryImages = new List<InventoryImageModel>();
                }

            }
            return View(model);
        }
        [HttpPost]
        public GalleryResult AddGalleryImage(GalleryImageModel model)
        {

            var userId = @User.Claims.FirstOrDefault(c => c.Type == "http://schemas.xmlsoap.org/ws/2005/05/identity/claims/nameidentifier").Value;
            GalleryResult result = new GalleryResult();
            result.status = "error";
            string fileName = model.InvId + "_" + DateTime.Now.Day + DateTime.Now.Month + DateTime.Now.Year + '_' + model.InventoryImage.FileName;
            string FilePath = FileUploadUtility.UploadFile(model.InventoryImage, Convert.ToString(model.InvCompanyID), fileName);
            List<KeyValuePair<string, string>> parameters = new List<KeyValuePair<string, string>>();
            parameters.Add(new KeyValuePair<string, string>("@userId", userId));
            parameters.Add(new KeyValuePair<string, string>("@InvId", Convert.ToString(model.InvId)));
            parameters.Add(new KeyValuePair<string, string>("@FilePath", Convert.ToString(FilePath)));
            parameters.Add(new KeyValuePair<string, string>("@FileName", fileName));
            DataTable dt = _objDataHelper.ExecuteProcedure("app_Add_Inventory_Gallery_Image", parameters);
            if (dt != null && dt.Rows.Count > 0)
            {
                result.status = Convert.ToString(dt.Rows[0]["success"]);
                result.imageId = Convert.ToInt32(dt.Rows[0]["imageId"]);
            }
            string returnResult = "/" + FilePath + "/" + fileName;
            result.imagePath = returnResult;
            return result;
        }
        public DataTable InventoryList()
        {
            var userId = @User.Claims.FirstOrDefault(c => c.Type == "http://schemas.xmlsoap.org/ws/2005/05/identity/claims/nameidentifier").Value;
            var role = @User.Claims.FirstOrDefault(c => c.Type == "http://schemas.microsoft.com/ws/2008/06/identity/claims/role").Value;
            List<KeyValuePair<string, string>> parameters = new List<KeyValuePair<string, string>>();
            parameters.Add(new KeyValuePair<string, string>("@userId", userId));
            parameters.Add(new KeyValuePair<string, string>("@role", role));
            return _objDataHelper.ExecuteProcedure("app_Get_All_Inventory", parameters);
        }
        private DataTable CompanyList()
        {
            var userId = @User.Claims.FirstOrDefault(c => c.Type == "http://schemas.xmlsoap.org/ws/2005/05/identity/claims/nameidentifier").Value;
            List<KeyValuePair<string, string>> parameters = new List<KeyValuePair<string, string>>();
            parameters.Add(new KeyValuePair<string, string>("@userId", userId));
            return _objDataHelper.ExecuteProcedure("app_Get_Companies_Created_By_Admin_User_Dropdow", parameters);
        }
        private DataTable BrandList()
        {
            return _objDataHelper.ExecuteProcedure("app_Get_All_Brands_For_Dropdown", null);
        }
        private DataTable ColorList()
        {
            return _objDataHelper.ExecuteProcedure("app_Get_All_Colors_For_Dropdown", null);
        }
        private DataTable InventoryConditionList()
        {
            return _objDataHelper.ExecuteProcedure("app_Get_All_InvCondition_For_Dropdown", null);
        }
    }
}