﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using ListKong.Models;
using ListKong.Utility;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;

namespace ListKong.Controllers
{
    public class CompanyUserController : Controller
    {
        #region Controller Properties
        private IConfiguration _configuration;
        private CommonHelper _objHelper;
        private SQLGateway _objDataHelper;
        public string cnnect;
        public string keyForEncrypt, encryptedToken, encryptedEmail, formFlag;
        private readonly UserManager<ApplicationUser> _userManager;

        public CompanyUserController(IConfiguration configuration, UserManager<ApplicationUser> userManager)
        {
            this._configuration = configuration;
            this._objDataHelper = new SQLGateway(this._configuration.GetConnectionString("DefaultConnection"));
            this._objHelper = new CommonHelper(this._configuration);
            cnnect = this._configuration.GetConnectionString("DefaultConnection");
            this._userManager = userManager;
        }
        #endregion
        public IActionResult Index()
        {
            string companyName = HttpContext.Session.GetString("_CompanyName");
            if (string.IsNullOrEmpty(companyName))
            {
                return RedirectToAction("Index", "Company");
            }
            DataTable _dtResponseP = CompanyUserListByUserId();
            if (this._objHelper.checkDBNullResponse(_dtResponseP))
            {
                List<CompanyUserListModel> lst = new List<CompanyUserListModel>();
                lst = ExtensionMethods.DataTableToList<CompanyUserListModel>(_dtResponseP);
                return View(lst);
            }

            return View();
        }
        public IActionResult CreateUser()
        {
            var loggedInUserId = @User.Claims.FirstOrDefault(c => c.Type == "http://schemas.xmlsoap.org/ws/2005/05/identity/claims/nameidentifier").Value;
            CompanyUserModel model = new CompanyUserModel();
            ViewData["Roles"] = RoleList();
            ViewData["CompanyName"] = HttpContext.Session.GetString("_CompanyName");
            var companyId= HttpContext.Session.GetString("_CompanyId");
            ViewData["Locations"] = LocationList();
            model.CompanyId = Convert.ToInt64(companyId);
            model.CompanyName = Convert.ToString(ViewData["CompanyName"]);
            //ViewData["CompanyByAdminUser"] = CompanyList();
            return View(model);
        }
        [HttpPost]
        public IActionResult CreateUser(CompanyUserModel model)
        {
            if (!ModelState.IsValid)
            {
                var loggedInUserId = @User.Claims.FirstOrDefault(c => c.Type == "http://schemas.xmlsoap.org/ws/2005/05/identity/claims/nameidentifier").Value;
                ViewData["Roles"] = RoleList();
                ViewData["CompanyName"] = HttpContext.Session.GetString("_CompanyName");
                var companyId = HttpContext.Session.GetString("_CompanyId");
                model.CompanyId = Convert.ToInt64(companyId);
                model.CompanyName = Convert.ToString(ViewData["CompanyName"]);
                ViewData["Locations"] = LocationList();
                return View(model);
            }
            var user = new ApplicationUser { UserName = model.Email, Email = model.Email, FirstName = model.FirstName, LastName = model.LastName };
            var result = _userManager.CreateAsync(user, model.Password).Result;
            if (result.Succeeded)
            {
                //bool adminRoleExists = _userManager.
                var role = _userManager.AddToRoleAsync(user, model.RoleName).Result;
                var currentUser = _userManager.FindByNameAsync(user.UserName).Result;
                var output = AddUserInCompany(currentUser.Id.ToString(), model.CompanyId,Convert.ToInt64(model.LocationId));
                TempData["SuccessMessage"] = "Company user successfully added!";
                return RedirectToAction("Index");
            }
            else
            {
                ViewData["Roles"] = RoleList();
                ViewData["CompanyName"] = HttpContext.Session.GetString("_CompanyName");
                var companyId = HttpContext.Session.GetString("_CompanyId");
                model.CompanyId = Convert.ToInt64(companyId);
                model.CompanyName = Convert.ToString(ViewData["CompanyName"]);
                ViewData["Locations"] = LocationList();
                TempData["ErrorMessage"] = result.Errors.FirstOrDefault().Description;
                return View(model);
            }
        }
        public IActionResult Edit(string Id)
        {
            EditCompanyUserModel model = new EditCompanyUserModel();
            ViewData["Roles"] = RoleList();
            ViewData["CompanyName"] = HttpContext.Session.GetString("_CompanyName");
            var companyId = HttpContext.Session.GetString("_CompanyId");
            model.CompanyId = Convert.ToInt64(companyId);
            model.CompanyName = Convert.ToString(ViewData["CompanyName"]);
            ViewData["Locations"] = LocationList();
            DataTable _dtResponseP = CompanyUserDetailById(Id);
            if (this._objHelper.checkDBNullResponse(_dtResponseP))
            {
                List<EditCompanyUserModel> lst = new List<EditCompanyUserModel>();
                lst = ExtensionMethods.DataTableToList<EditCompanyUserModel>(_dtResponseP);
                return View(lst.FirstOrDefault());
            }
            return View(model);
        }
        [HttpPost]
        public IActionResult Edit(EditCompanyUserModel model)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    ViewData["Roles"] = RoleList();
                    ViewData["CompanyName"] = HttpContext.Session.GetString("_CompanyName");
                    var companyId = HttpContext.Session.GetString("_CompanyId");
                    model.CompanyId = Convert.ToInt64(companyId);
                    model.CompanyName = Convert.ToString(ViewData["CompanyName"]);
                    ViewData["Locations"] = LocationList();
                    return View(model);
                }
                ViewData["Roles"] = RoleList();
                ViewData["CompanyByAdminUser"] = CompanyList();

                var loggedInUserId = @User.Claims.FirstOrDefault(c => c.Type == "http://schemas.xmlsoap.org/ws/2005/05/identity/claims/nameidentifier").Value;
                List<KeyValuePair<string, string>> parameters = new List<KeyValuePair<string, string>>();
                parameters.Add(new KeyValuePair<string, string>("@Id", model.Id));
                parameters.Add(new KeyValuePair<string, string>("@FirstName", model.FirstName));
                parameters.Add(new KeyValuePair<string, string>("@LastName", model.LastName));
                parameters.Add(new KeyValuePair<string, string>("@Email", model.Email));
                parameters.Add(new KeyValuePair<string, string>("@RoleName", model.RoleName));
                parameters.Add(new KeyValuePair<string, string>("@CompanyId", Convert.ToString(model.CompanyId)));
                parameters.Add(new KeyValuePair<string, string>("@LocationId", Convert.ToString(model.LocationId)));
                parameters.Add(new KeyValuePair<string, string>("@UpdatedBy", Convert.ToString(loggedInUserId)));
                DataTable dt = _objDataHelper.ExecuteProcedure("app_Update_User_Detail_Created_By_Admin", parameters);
                TempData["SuccessMessage"] = "User successfully Updated!";
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                TempData["ErrorMessage"] = ex.Message.ToString();
                return RedirectToAction("Index");
            }
        }
        public async Task<IActionResult> Delete(string Id)
        {
            EditCompanyUserModel model = new EditCompanyUserModel();
            try
            {
                DataTable _dtResponseP = DeleteUser(Id);
                if (this._objHelper.checkDBNullResponse(_dtResponseP))
                {
                    if (Convert.ToString(_dtResponseP.Rows[0]["Result"]) == "1")
                    {
                        TempData["SuccessMessage"] = "User successfully Deleted!";
                        return RedirectToAction("Index");
                    }
                    else
                    {
                        TempData["ErrorMessage"] = "There is some error. Please try later!";
                        return RedirectToAction("Index");
                    }
                }
                return View(model);
            }
            catch (Exception ex)
            {
                TempData["ErrorMessage"] = ex.Message.ToString();
                return RedirectToAction("Index");
            }
        }
        private int AddUserInCompany(string userId, Int64 CompanyId,Int64 LocationId)
        {
            var loggedInUserId = @User.Claims.FirstOrDefault(c => c.Type == "http://schemas.xmlsoap.org/ws/2005/05/identity/claims/nameidentifier").Value;
            List<KeyValuePair<string, string>> parameters = new List<KeyValuePair<string, string>>();
            parameters.Add(new KeyValuePair<string, string>("@UserId", userId));
            parameters.Add(new KeyValuePair<string, string>("@CompanyId", Convert.ToString(CompanyId)));
            parameters.Add(new KeyValuePair<string, string>("@LocationId", Convert.ToString(LocationId)));
            parameters.Add(new KeyValuePair<string, string>("@CreatedBy", Convert.ToString(loggedInUserId)));
            DataTable dt = _objDataHelper.ExecuteProcedure("app_Add_New_Company_User", parameters);
            return 1;
        }

        private DataTable CompanyUserListByUserId()
        {
            var userId = @User.Claims.FirstOrDefault(c => c.Type == "http://schemas.xmlsoap.org/ws/2005/05/identity/claims/nameidentifier").Value;

            List<KeyValuePair<string, string>> parameters = new List<KeyValuePair<string, string>>();
            parameters.Add(new KeyValuePair<string, string>("@userId", userId));
            return _objDataHelper.ExecuteProcedure("app_Get_All_User_Created_By_Admin_User", parameters);
        }
        private DataTable CompanyUserDetailById(string @comlocId)
        {
            //var userId = @User.Claims.FirstOrDefault(c => c.Type == "http://schemas.xmlsoap.org/ws/2005/05/identity/claims/nameidentifier").Value;
            List<KeyValuePair<string, string>> parameters = new List<KeyValuePair<string, string>>();
            parameters.Add(new KeyValuePair<string, string>("@comlocId", comlocId));
            return _objDataHelper.ExecuteProcedure("app_Get_User_Detail_Created_By_Admin_By_Id", parameters);
        }
        private DataTable RoleList()
        {
            return _objDataHelper.ExecuteProcedure("app_Get_All_Role_For_Dropdown", null);
        }
        private DataTable CompanyList()
        {
            var userId = @User.Claims.FirstOrDefault(c => c.Type == "http://schemas.xmlsoap.org/ws/2005/05/identity/claims/nameidentifier").Value;
            List<KeyValuePair<string, string>> parameters = new List<KeyValuePair<string, string>>();
            parameters.Add(new KeyValuePair<string, string>("@userId", userId));
            return _objDataHelper.ExecuteProcedure("app_Get_Companies_Created_By_Admin_User_Dropdow", parameters);
        }
        public DataTable LocationList()
        {
            var userId = @User.Claims.FirstOrDefault(c => c.Type == "http://schemas.xmlsoap.org/ws/2005/05/identity/claims/nameidentifier").Value;
            List<KeyValuePair<string, string>> parameters = new List<KeyValuePair<string, string>>();
            parameters.Add(new KeyValuePair<string, string>("@userId", userId));
            return _objDataHelper.ExecuteProcedure("app_Get_All_Location_By_Admin_User", parameters);
        }
        private DataTable DeleteUser(string Id)
        {
            var userId = @User.Claims.FirstOrDefault(c => c.Type == "http://schemas.xmlsoap.org/ws/2005/05/identity/claims/nameidentifier").Value;
            List<KeyValuePair<string, string>> parameters = new List<KeyValuePair<string, string>>();
            parameters.Add(new KeyValuePair<string, string>("@Id", Id));
            parameters.Add(new KeyValuePair<string, string>("@userId", userId));
            return _objDataHelper.ExecuteProcedure("app_Delete_User_By_Id", parameters);
        }
    }
}