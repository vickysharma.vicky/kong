﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using ListKong.Models;
using ListKong.Utility;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;

namespace ListKong.Controllers
{
    public class LocationController : Controller
    {
        #region Controller Properties
        private IConfiguration _configuration;
        private CommonHelper _objHelper;
        private SQLGateway _objDataHelper;
        public string cnnect;
        public string keyForEncrypt, encryptedToken, encryptedEmail, formFlag;
        #endregion
        public LocationController(IConfiguration configuration)
        {
            this._configuration = configuration;
            this._objDataHelper = new SQLGateway(this._configuration.GetConnectionString("DefaultConnection"));
            this._objHelper = new CommonHelper(this._configuration);
            cnnect = this._configuration.GetConnectionString("DefaultConnection");
        }
        public IActionResult Index()
        {
            string companyName = HttpContext.Session.GetString("_CompanyName");
            if (string.IsNullOrEmpty(companyName))
            {
                return RedirectToAction("Index", "Company");
            }
            try
            {
                DataTable _dtResponseP = LocationList();
                if (this._objHelper.checkDBNullResponse(_dtResponseP))
                {
                    List<LocationListModel> lst = new List<LocationListModel>();
                    lst = ExtensionMethods.DataTableToList<LocationListModel>(_dtResponseP);
                    return View(lst);
                }
            }
            catch (Exception ex)
            {
                string E = ex.Message.ToString();
                return View();
            }
            return View();
        }
        public async Task<IActionResult> AddLocation()
        {
            LocationModel model = new LocationModel();
            model.CompanyId = Convert.ToInt64(HttpContext.Session.GetString("_CompanyId"));
            model.CompanyName = HttpContext.Session.GetString("_CompanyName");
            return View(model);
        }
        [HttpPost]
        public async Task<IActionResult> AddLocation(LocationModel model)
        {

            if (!ModelState.IsValid)
            {
                return View(model);
            }
            try
            {
                var userId = @User.Claims.FirstOrDefault(c => c.Type == "http://schemas.xmlsoap.org/ws/2005/05/identity/claims/nameidentifier").Value;
                List<KeyValuePair<string, string>> parameters = new List<KeyValuePair<string, string>>();
                parameters.Add(new KeyValuePair<string, string>("@userId", userId));
                parameters.Add(new KeyValuePair<string, string>("@CompanyId", Convert.ToString(model.CompanyId)));
                parameters.Add(new KeyValuePair<string, string>("@City", model.City));
                parameters.Add(new KeyValuePair<string, string>("@Country", model.Country));
                parameters.Add(new KeyValuePair<string, string>("@LocationAddress1", model.LocationAddress1));
                parameters.Add(new KeyValuePair<string, string>("@LocationAddress2", model.LocationAddress2));
                parameters.Add(new KeyValuePair<string, string>("@LocationDescription", model.LocationDescription));
                parameters.Add(new KeyValuePair<string, string>("@LocationName", model.LocationName));
                parameters.Add(new KeyValuePair<string, string>("@State", model.State));
                parameters.Add(new KeyValuePair<string, string>("@ZipCode", model.ZipCode));
                DataTable dt = _objDataHelper.ExecuteProcedure("app_Add_New_Location_By_Company", parameters);
                if (dt != null && dt.Rows.Count > 0)
                {
                    if (Convert.ToString(dt.Rows[0]["Result"]) == "0")
                    {
                        ViewData["ErrorMessage"] = "Location already exists!";
                        return View(model);
                    }
                    else
                    {

                        TempData["SuccessMessage"] = "Location successfully added!";
                        return RedirectToAction("Index");
                    }
                }
                else
                {
                    ViewData["ErrorMessage"] = "There is some error. Please try later!";
                    return View(model);
                }

                //RedirectToAction("AddInventory");

            }
            catch (Exception ex)
            {
                ViewData["ErrorMessage"] = "There is some error. Please try later!";
                string msg = ex.Message.ToString();
            }
            return View(model);
        }
        public string SetDefaultLocation(string Id)
        {
            SetDefaultLoc(Id);
            return "Ok";
        }

        public async Task<IActionResult> Edit(string Id)
        {
            LocationModel model = new LocationModel();
            try
            {
                DataTable _dtResponseP = LocationDetailById(Id);
                if (this._objHelper.checkDBNullResponse(_dtResponseP))
                {
                    List<LocationModel> lst = new List<LocationModel>();
                    lst = ExtensionMethods.DataTableToList<LocationModel>(_dtResponseP);
                    return View(lst.FirstOrDefault());
                }
                return View(model);
            }
            catch (Exception ex)
            {
                string E = ex.Message.ToString();
                return View(model);
            }
        }
        [HttpPost]
        public async Task<IActionResult> Edit(LocationModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }
            try
            {
                var userId = @User.Claims.FirstOrDefault(c => c.Type == "http://schemas.xmlsoap.org/ws/2005/05/identity/claims/nameidentifier").Value;
                List<KeyValuePair<string, string>> parameters = new List<KeyValuePair<string, string>>();
                parameters.Add(new KeyValuePair<string, string>("@userId", userId));
                parameters.Add(new KeyValuePair<string, string>("@locationId", Convert.ToString(model.LocationId)));
                parameters.Add(new KeyValuePair<string, string>("@CompanyId", Convert.ToString(model.CompanyId)));
                parameters.Add(new KeyValuePair<string, string>("@City", model.City));
                parameters.Add(new KeyValuePair<string, string>("@Country", model.Country));
                parameters.Add(new KeyValuePair<string, string>("@LocationAddress1", model.LocationAddress1));
                parameters.Add(new KeyValuePair<string, string>("@LocationAddress2", model.LocationAddress2));
                parameters.Add(new KeyValuePair<string, string>("@LocationDescription", model.LocationDescription));
                parameters.Add(new KeyValuePair<string, string>("@LocationName", model.LocationName));
                parameters.Add(new KeyValuePair<string, string>("@State", model.State));
                parameters.Add(new KeyValuePair<string, string>("@ZipCode", model.ZipCode));
                DataTable dt = _objDataHelper.ExecuteProcedure("app_Update_Location_By_Id", parameters);
                if (dt != null && dt.Rows.Count > 0)
                {
                    if (Convert.ToString(dt.Rows[0]["Result"]) == "0")
                    {
                        ViewData["ErrorMessage"] = "Location already exists!";
                        return View(model);
                    }
                    else
                    {

                        TempData["SuccessMessage"] = "Location successfully Updated!";
                        return RedirectToAction("Index");
                    }
                }
                else
                {
                    ViewData["ErrorMessage"] = "There is some error. Please try later!";
                    return View(model);
                }

                //RedirectToAction("AddInventory");

            }
            catch (Exception ex)
            {
                ViewData["ErrorMessage"] = "There is some error. Please try later!";
                string msg = ex.Message.ToString();
            }
            return View(model);
        }
        public async Task<IActionResult> Delete(string Id)
        {
            LocationModel model = new LocationModel();
            try
            {
                DataTable _dtResponseP = DeleteLocation(Id);
                if (this._objHelper.checkDBNullResponse(_dtResponseP))
                {
                    if (Convert.ToString(_dtResponseP.Rows[0]["Result"]) == "1")
                    {
                        TempData["SuccessMessage"] = "Location successfully Deleted!";
                        return RedirectToAction("Index");
                    }
                    else
                    {
                        TempData["ErrorMessage"] = "There is some error. Please try later!";
                        return RedirectToAction("Index");
                    }
                }
                return View(model);
            }
            catch (Exception ex)
            {
                TempData["ErrorMessage"] = ex.Message.ToString();
                return RedirectToAction("Index");
            }
        }
        public DataTable LocationList()
        {
            var userId = @User.Claims.FirstOrDefault(c => c.Type == "http://schemas.xmlsoap.org/ws/2005/05/identity/claims/nameidentifier").Value;
            List<KeyValuePair<string, string>> parameters = new List<KeyValuePair<string, string>>();
            parameters.Add(new KeyValuePair<string, string>("@userId", userId));
            return _objDataHelper.ExecuteProcedure("app_Get_All_Location_By_Admin_User", parameters);
        }
        private DataTable LocationDetailById(string Id)
        {
            //var userId = @User.Claims.FirstOrDefault(c => c.Type == "http://schemas.xmlsoap.org/ws/2005/05/identity/claims/nameidentifier").Value;
            List<KeyValuePair<string, string>> parameters = new List<KeyValuePair<string, string>>();
            parameters.Add(new KeyValuePair<string, string>("@Id", Id));
            return _objDataHelper.ExecuteProcedure("app_Get_Location_Detail_By_Id", parameters);
        }
        private DataTable DeleteLocation(string Id)
        {
            var userId = @User.Claims.FirstOrDefault(c => c.Type == "http://schemas.xmlsoap.org/ws/2005/05/identity/claims/nameidentifier").Value;
            List<KeyValuePair<string, string>> parameters = new List<KeyValuePair<string, string>>();
            parameters.Add(new KeyValuePair<string, string>("@Id", Id));
            parameters.Add(new KeyValuePair<string, string>("@userId", userId));
            return _objDataHelper.ExecuteProcedure("app_Delete_Location_By_Id", parameters);
        }
        private DataTable SetDefaultLoc(string Id)
        {
            var userId = @User.Claims.FirstOrDefault(c => c.Type == "http://schemas.xmlsoap.org/ws/2005/05/identity/claims/nameidentifier").Value;
            List<KeyValuePair<string, string>> parameters = new List<KeyValuePair<string, string>>();
            parameters.Add(new KeyValuePair<string, string>("@Id", Id));
            parameters.Add(new KeyValuePair<string, string>("@userId", userId));
            return _objDataHelper.ExecuteProcedure("app_Set_Default_Location", parameters);
        }
    }
}