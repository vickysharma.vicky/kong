﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using ListKong.Models;
using ListKong.Utility;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;

namespace ListKong.Controllers
{
    [Authorize]
    public class ReportsController : Controller
    {
        #region Controller Properties
        private IConfiguration _configuration;
        private CommonHelper _objHelper;
        private SQLGateway _objDataHelper;
        public string cnnect;
        public string keyForEncrypt, encryptedToken, encryptedEmail, formFlag;

        public ReportsController(IConfiguration configuration)
        {
            this._configuration = configuration;
            this._objDataHelper = new SQLGateway(this._configuration.GetConnectionString("DefaultConnection"));
            this._objHelper = new CommonHelper(this._configuration);
            cnnect = this._configuration.GetConnectionString("DefaultConnection");
        }
        #endregion
        public IActionResult InventoryReport()
        {
            try
            {
                string companyName = HttpContext.Session.GetString("_CompanyName");
                this.ViewData["companyName"] = companyName;
                InventoryReportModel model = new InventoryReportModel();
                model.listing = new List<InventoryListReportModel>();
                return View(model);
                //DataTable _dtResponseP = InventoryList();
                //if (this._objHelper.checkDBNullResponse(_dtResponseP))
                //{
                //    List<InventoryListModel> lst = new List<InventoryListModel>();
                //    lst = ExtensionMethods.DataTableToList<InventoryListModel>(_dtResponseP);
                //    return View(lst);
                //}
            }
            catch (Exception ex)
            {
                string E = ex.Message.ToString();
                return View();
            }
            return View();
        }
        [HttpPost]
        public IActionResult InventoryReport(InventoryReportModel model)
        {
            string companyName = HttpContext.Session.GetString("_CompanyName");
            this.ViewData["companyName"] = companyName;
            try
            {
                if (!string.IsNullOrEmpty(Convert.ToString(model.startDate)) && !string.IsNullOrEmpty(Convert.ToString(model.startTime)))
                {
                    TimeSpan tsStartTime = DateTime.Parse(model.startTime).TimeOfDay;
                    TimeSpan tsEndTime = DateTime.Parse(model.endTime).TimeOfDay;
                    DateTime sdate = Convert.ToDateTime(model.startDate);
                    DateTime edate = Convert.ToDateTime(model.endDate);
                    sdate = Convert.ToDateTime(sdate.Add(tsStartTime));
                    edate = Convert.ToDateTime(edate.Add(tsEndTime));
                    //DateTime sDate=model.startDate.Value.t
                    DataTable _dtResponseP = InventoryList(sdate, edate);
                    if (this._objHelper.checkDBNullResponse(_dtResponseP))
                    {
                        List<InventoryListReportModel> lst = new List<InventoryListReportModel>();
                        lst = ExtensionMethods.DataTableToList<InventoryListReportModel>(_dtResponseP);
                        model.listing = lst;
                        return View(model);
                    }
                }
                else
                {
                    model.listing = new List<InventoryListReportModel>();
                    return View(model);
                }
            }
            catch (Exception ex)
            {
                string E = ex.Message.ToString();
                return View(model);
            }
            return View(model);
        }
        public DataTable InventoryList(DateTime startDate, DateTime endDate)
        {
            var userId = @User.Claims.FirstOrDefault(c => c.Type == "http://schemas.xmlsoap.org/ws/2005/05/identity/claims/nameidentifier").Value;
            var role = @User.Claims.FirstOrDefault(c => c.Type == "http://schemas.microsoft.com/ws/2008/06/identity/claims/role").Value;
            List<KeyValuePair<string, object>> parameters = new List<KeyValuePair<string, object>>();
            parameters.Add(new KeyValuePair<string, object>("@userId", userId));
            parameters.Add(new KeyValuePair<string, object>("@role", role));
            parameters.Add(new KeyValuePair<string, object>("@startDate", startDate));
            parameters.Add(new KeyValuePair<string, object>("@endDate", endDate));
            return _objDataHelper.ExecuteProcedureWithObject("app_Get_All_Inventory_Report", parameters);
        }
    }
}
